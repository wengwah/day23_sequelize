
module.exports = function(conn, Sequelize) {
    var Employees=  conn.define('employees', {
        emp_no: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        birth_date: {
            type: Sequelize.DATE,
            allowNull: false
        },
        first_name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        last_name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        gender: {
            type: Sequelize.ENUM('M','F'),
            allowNull: false
        },
        hire_date: {
            type: Sequelize.DATE,
            allowNull: false
        }
    }, {

        // Validation on the server side
        // validate: {
        //   checkEmployeeNoAndGender() {
        //     if(!(this.last_name > 10) && !(this.gender === 'F')) {
        //       throw new Error("ERROR! Must be employee no > 10 and 
        //         gender equals female");
        //     }
        //   }
        // },

        // By default, sequelize creates singular table names
        tableName: 'employees', 
        timestamps: false
    });
    return Employees;
};